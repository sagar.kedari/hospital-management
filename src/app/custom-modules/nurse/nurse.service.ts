import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IDoctorList } from 'src/app/interface/app.types';

@Injectable({
  providedIn: 'root'
})
export class NurseService {

  baseUrl: string = 'http://localhost:3000';
  constructor(private http:HttpClient) { }

  getDoctorName(){
    return this.http.get(`${this.baseUrl}/user/profile`)
  }
  postMessage(form:any):Observable<IDoctorList>{
    return this.http.post(`${this.baseUrl}/message`,form) as Observable<IDoctorList>
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NurseHomeComponent } from './nurse-home/nurse-home.component';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import { NurseRoutingModule } from './nurse-home/nurse-routing.module';
import {MatFormFieldModule} from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';




@NgModule({
  declarations: [
    NurseHomeComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatFormFieldModule,
    NurseRoutingModule,
    MatTableModule,
    ReactiveFormsModule
  ]
})
export class NurseModule { }

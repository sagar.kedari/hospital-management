import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-nurse-list',
  templateUrl: './nurse-list.component.html',
  styleUrls: ['./nurse-list.component.scss']
})
export class NurseListComponent implements OnInit {
  columnsToBeDisplayed: string[] = ['name', 'doctor','edit','delete'];
  dataSource = new MatTableDataSource<any>;
  constructor() { }

  ngOnInit(): void {
    this.getNurseList();
  }

  getNurseList(){
    
  }
}

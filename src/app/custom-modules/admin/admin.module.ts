import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoctorListComponent } from './doctor-list/doctor-list.component';
import { NurseListComponent } from './nurse-list/nurse-list.component';
import { RequestsComponent } from './requests/requests.component';
import { NotificationComponent } from './notification/notification.component';
import { ShareModule } from '../share/share.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatPaginatorModule} from '@angular/material/paginator';
import { AdminRoutingModule } from './admin-routing.module';
import {MatFormFieldModule} from '@angular/material/form-field';
// import { MatSelectModule, MatOptionModule} from '@angular/material'



@NgModule({
  declarations: [
    DoctorListComponent,
    NurseListComponent,
    RequestsComponent,
    NotificationComponent
  ],
  imports: [
    CommonModule,
    ShareModule,
    ReactiveFormsModule,
    MatTableModule,
    MatIconModule,
    MatPaginatorModule,
    AdminRoutingModule,
    MatFormFieldModule,
    FormsModule
    // MatOptionModule,

  ]
})
export class AdminModule { }

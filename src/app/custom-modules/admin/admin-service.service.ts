import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IDoctorList } from 'src/app/interface/app.types';


@Injectable({
  providedIn: 'root'
})
export class AdminServiceService {
  baseUrl:string = 'http://localhost:3000'

  constructor(private http:HttpClient) { }
  doctorList:IDoctorList[]=[]

  getDoctorList():Observable<IDoctorList>{
    return this.http.get<IDoctorList>(`${this.baseUrl}/user`) as Observable<IDoctorList>
  }
  deleteDoctorData(id:string){  
    return this.http.delete(`${this.baseUrl}/user/${id}`)
  }
  postData(form:any){
    return this.http.post(`${this.baseUrl}/user/register`,form)
  }
}

import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { DoctorService } from '../doctor.service';

@Component({
  selector: 'app-change-request',
  templateUrl: './change-request.component.html',
  styleUrls: ['./change-request.component.scss']
})
export class ChangeRequestComponent implements OnInit {

  constructor(private requestService:DoctorService) { }

  ngOnInit(): void {
  }
  columnsToBeDisplayed=['request','status']
  dataSource = new MatTableDataSource<any>;
  displayRequest(){
    this.requestService.getRequestList().subscribe(res=>{
      console.log(res)
    })
  }
}

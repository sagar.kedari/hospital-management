import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { ChangeRequestComponent } from './change-request/change-request.component';
import { MessagesComponent } from './messages/messages.component';
import { DoctorRoutingModule } from './doctor-routing.module';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatPaginatorModule} from '@angular/material/paginator';
import { ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';

@NgModule({
  declarations: [
    HomeComponent,
    ChangeRequestComponent,
    MessagesComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    DoctorRoutingModule,
    ReactiveFormsModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule
  ]
})
export class DoctorModule { }

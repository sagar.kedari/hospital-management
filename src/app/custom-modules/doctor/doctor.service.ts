import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IDoctorList, INurse } from 'src/app/interface/app.types';

@Injectable({
  providedIn: 'root',
})
export class DoctorService {
  baseUrl: string = 'http://localhost:3000';
  constructor(private http: HttpClient) {}

  getNurseList() {
    return this.http.get(`${this.baseUrl}/user`) as Observable<IDoctorList>;
  }
  changeNurse(nurseData:INurse){
    console.log(nurseData)
    return this.http.post(`${this.baseUrl}/change-request`, nurseData)
  }
  getRequestList(){
    return this.http.get(`${this.baseUrl}/change-request`)
  }
}

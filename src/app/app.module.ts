import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPageComponent } from './custom-modules/auth/login-page/login-page.component';
import { AuthModule } from './custom-modules/auth/auth.module';
import { ShareModule } from './custom-modules/share/share.module';
import { HttpClient, HttpClientModule, HttpClientXsrfModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthService } from './custom-modules/auth/services/auth.service';
import { InterceptorService } from './interceptor';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AuthModule,
    ShareModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [AuthService, {
    provide: HTTP_INTERCEPTORS,
    useClass:InterceptorService,
    multi:true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
